<?php
/**
 * Implements hook_form()
 * Settings form for Salesforce Outbound messaging
 *
 * @param $form
 * @param $form_state
 * @return mixed
 * @see salesforce_outbound_messaging_settings_form_validate
 */
function salesforce_outbound_messaging_settings_form($form, &$form_state) {
  $form['salesforce_outbound_messaging_endpoint'] = array(
    '#type' => 'textfield',
    '#title' => t('Endpoint path'),
    '#default_value' => variable_get('salesforce_outbound_messaging_endpoint', 'salesforce/notifications'),
    '#required' => TRUE,
    '#maxlength' => 128,
    '#description' => t('Endpoint path for the incoming Salesforce outbound messages'),
  );

  // IP Whitelist form
  $form['salesforce_outbound_messaging_ip_whitelist'] = array(
    '#type' => 'fieldset',
    '#title' => t('IP Whitelist'),
    '#description' => t('Settings for an IP whitelist of valid Salesforce IPs.'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer salesforce'),
  );

  $form['salesforce_outbound_messaging_ip_whitelist']['salesforce_outbound_messaging_allowed_ips'] = array(
    '#type' => 'textarea',
    '#title' => t('Allowed IP addresses'),
    '#description' => t('Enter the <a href="@IPs">Salesforce IP addresses</a> that will send outbound messages to your site. Enter one IP address or CIDR mask per line.', array('@IPs' => url('http://help.salesforce.com/apex/HTViewSolution?id=000003652'))),
    '#cols' => 60,
    '#rows' => 5,
    '#default_value' => variable_get('salesforce_outbound_messaging_allowed_ips', implode("\n", salesforce_outbound_messaging_default_allowed_ips())),
  );

  // Queue Settings form
  $queue = DrupalQueue::get('salesforce_outbound_messaging_queue');

  $form['salesforce_outbound_messaging_queue'] = array(
    '#type' => 'fieldset',
    '#title' => t('Incoming Notifications Queue'),
    '#description' => t('Settings for the queueing of incoming notifications.<br/> There are @count items currently in the queue.', array('@count' => $queue->numberOfItems())),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#access' => user_access('administer salesforce'),
  );

  $form['salesforce_outbound_messaging_queue']['salesforce_outbound_messaging_use_queue'] = array(
    '#type' => 'checkbox',
    '#title' => t('Queue incoming salesforce messages.'),
    '#default_value' => variable_get('salesforce_outbound_messaging_use_queue', 0),
    '#description' => t('By placing incoming notifications into a queue, your site can stand up to the odd time when Salesforce sends a large volume of outbound notifications at once. However, the incoming messages will not be processed immediately.'),
  );

  $form['salesforce_outbound_messaging_debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Log incoming notifications in watchdog.'),
    '#default_value' => variable_get('salesforce_outbound_messaging_debug_mode', 1),
    '#description' => t('Enabled by default, this will log all incoming notifications with their content in the watchdog.'),
  );

  return system_settings_form($form);
}

/**
 * Implements hook_validate()
 *
 * Validates the endpoint path
 *
 * @param $form
 * @param $form_state
 * @see salesforce_outbound_messaging_settings_form
 */
function salesforce_outbound_messaging_settings_form_validate($form, &$form_state){
  // Check if the url is already taken
  $result = db_select('menu_router', 'm')
    ->fields('m', array('page_callback'))
    ->condition('path', $form_state['values']['salesforce_outbound_messaging_endpoint'])
    ->execute()
    ->fetchField();
  if(isset($result) AND $result != '' AND $result != 'salesforce_outbound_messaging_endpoint') {
    form_set_error('salesforce_outbound_messaging_endpoint', t('Path already taken by another menu entry'));
  }

  if(!valid_url($form_state['values']['salesforce_outbound_messaging_endpoint'])) {
    form_set_error('salesforce_outbound_messaging_endpoint', t('Invalid path format for endpoint'));
  }
}






